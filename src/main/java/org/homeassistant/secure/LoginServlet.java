package org.homeassistant.secure;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serial;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.homeassistant.secure.auth.InputCallbackHandler;
import org.homeassistant.secure.auth.LoginRequest;

import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;

@WebServlet(name = "LoginServlet", urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {
    @Serial
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter pw=response.getWriter();
        pw.println("<html body=\"red\">");
        pw.println("<center>");
        pw.println("<h1>Secure Home Assistant</h1>");
        pw.println("<p>Log in to your Home Assistant instance</p>");
        pw.println("<form action=\"/secure-home-assistant/login\" method=\"post\">");
        pw.println("<input type=\"text\" name=\"username\" placeholder=\"Username\">");
        pw.println("<input type=\"text\" name=\"password\" placeholder=\"Password\">");
        pw.println("<input type=\"submit\" value=\"login\" disabled=\"true\">");
        pw.println("</form>");
        pw.println("<center>");
        pw.println("</body></html>");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        PrintWriter out = response.getWriter();

        ObjectMapper mapper = new ObjectMapper();
        LoginContext lc = null;

        try {
            LoginRequest user = mapper.readValue(request.getReader(), LoginRequest.class);
            // out.println(user);  // TODO - exposes credentials !!!

            lc = new LoginContext("SecureHomeAssistant", new InputCallbackHandler());
            lc.login();
        } catch (IOException ignored) {
        } catch (LoginException | SecurityException le) {
            System.err.println("Cannot create LoginContext." + le.getMessage());
        }

        doGet(request, response);
    }
}