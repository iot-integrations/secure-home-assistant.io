package org.homeassistant.secure;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public class HomeAssistantApiImpl extends HomeAssistantApi {
    public HomeAssistantApiImpl() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        URL resource = getClass().getClassLoader().getResource("configuration.json");
        if (resource == null) {
            throw new RuntimeException("no ressorce");
        }
        try {
            HomeAssistantApi api = objectMapper.readValue(new File(resource.getFile()), HomeAssistantApi.class);
            this.accessToken = api.accessToken;
            this.url = api.url;
        } catch (IOException e) {
            System.out.println(e.getMessage());
            this.accessToken = "MISSING_TOKEN";
            this.url = "http://localhost:8123/api/";
        }
    }
}
