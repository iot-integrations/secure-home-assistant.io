package org.homeassistant.secure;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.servlet.GuiceServletContextListener;
import com.google.inject.servlet.ServletModule;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletContextEvent;

public class ServiceServletConfig extends GuiceServletContextListener {
    private ServletContext servletContext;
    @Override
    protected Injector getInjector() {
        return Guice.createInjector(new ServletModule() {
            @Override
            protected void configureServlets() {
                super.configureServlets();
                System.out.println("hitme");
                serve("/*").with(ServiceServlet.class);
                bind(HomeAssistantApi.class).to(HomeAssistantApiImpl.class);
            }
        });
    }

    @Override
    public void contextInitialized(ServletContextEvent event) {
        servletContext = event.getServletContext();
        super.contextInitialized(event);
    }

}
