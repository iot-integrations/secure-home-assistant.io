package org.homeassistant.secure;

import com.google.inject.AbstractModule;

public class BasicModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(HomeAssistantApi.class).to(HomeAssistantApiImpl.class);
    }
}
