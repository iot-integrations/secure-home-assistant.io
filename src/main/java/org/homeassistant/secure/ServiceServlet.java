package org.homeassistant.secure;

import java.io.IOException;
import java.io.Serial;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Provider;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.util.concurrent.TimeUnit;

@WebServlet(name = "ServiceServlet", urlPatterns = {"/services"})
public class ServiceServlet extends HttpServlet {
    @Serial
    private static final long serialVersionUID = 1L;

    @Inject
    private HomeAssistantApi apiConstants;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        // TODO - needs to come from config
        Injector injector = Guice.createInjector(new BasicModule());
        apiConstants = injector.getInstance(HomeAssistantApi.class);
    }

    private synchronized OkHttpClient getHttpClient() {
        return new OkHttpClient.Builder()
                .readTimeout(1000, TimeUnit.MILLISECONDS)
                .writeTimeout(1000, TimeUnit.MILLISECONDS)
                .build();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");

        Request req = new Request.Builder()
                .url(apiConstants.url + "services")
                .get()
                .addHeader("Authorization",
                        "Bearer " + apiConstants.accessToken)
                .build();

        String r;
        Call call = getHttpClient().newCall(req);
        try (Response resp = call.execute()) {
            r =  (resp.body() != null) ? resp.body().string() : "";
        }

        response.setStatus((!r.isEmpty()) ? 200 : 204);
        response.getWriter().print(r);
    }
}
