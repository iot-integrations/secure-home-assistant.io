# secure-home-assistant.io

## Getting started

- [ ] Install the embedded database SQLite

```
sudo apt update
sudo apt install sqlite3
sqlite3 secure-homeassistant.db
```

- [ ] Install Java 19 runtime environment

```
sudo apt install openjdk-19-jdk
```

# License

This installation package is licensed under the MIT opensource license.
